import org.junit.Test;

import java.io.Serializable;
import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.*;
import java.math.BigInteger;
import java.math.BigDecimal;

import scala.collection.JavaConverters;
import scala.collection.Seq;

import com.google.common.collect.ImmutableMap;
import com.google.common.primitives.Ints;
import org.junit.*;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.expressions.UserDefinedFunction;
import org.apache.spark.sql.types.*;
import org.apache.spark.util.sketch.BloomFilter;
import org.apache.spark.util.sketch.CountMinSketch;

import org.junit.Assert.*;

import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.Dataset;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.*;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

public class SimpleAppTest {

 private JavaSparkContext sparkCtx;

    @Before
    public void init() throws IllegalArgumentException, IOException {
        //ctxtBuilder = new ContextBuilder(tempFolder);
        SparkConf conf = new SparkConf();
        conf.setMaster("local[2]");
        conf.setAppName("junit");
        sparkCtx = new JavaSparkContext(conf);     
    }

    @Test
    public void test() {
        final List<String> s = new ArrayList<String>();
        s.add("1,1510670916247,10");
	s.add("1,1510670916249,50");
	s.add("1,1510670916251,50");
	s.add("2,1510670916253,20");
	s.add("1,1510670916255,10");

        JavaRDD<String> rdd = sparkCtx.parallelize(s,1);
	rdd = SimpleApp.Process(rdd);

	List<String> l = rdd.collect();

	assert(l.size() ==  2);

	String[] res = l.toArray(new String[l.size()]);

	System.out.println(res[0]);
	System.out.println(res[1]);

	assert(res[0].equals("1,1510670880000,1m,30"));
	assert(res[1].equals("2,1510670880000,1m,20"));
    }

}
