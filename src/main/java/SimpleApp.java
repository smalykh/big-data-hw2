/* SimpleApp.java */
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.Dataset;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.*;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import java.util.Arrays;
import java.lang.Object.*;
import java.util.*;
import org.apache.spark.api.java.*;
import org.apache.spark.api.java.function.*;
import scala.Tuple2;

public class SimpleApp {
	static long SCALE = 60*1000;
	static String STR_SCALE = "1m";

	//static String logFile = "hdfs://localhost:9000/log";
	static String logFile = "hdfs://localhost:9000/user/hadoop/Data";
	static String outFile = "hdfs://localhost:9000/out";


	public static JavaRDD<String> Process(JavaRDD<String> textFile)
	{
		JavaPairRDD<Tuple2<Integer, Long>, Tuple2<Integer,Integer>> counts = textFile
			.mapToPair(record -> new Tuple2<> (
							new Tuple2<> (Integer.parseInt(record.split(",")[0]), 
								 	Long.parseLong(record.split(",")[1])/SCALE*SCALE), 
							new Tuple2 <> (1, Integer.parseInt(record.split(",")[2]))
						)
			)
			.reduceByKey((a, b) -> new Tuple2<>(a._1 + b._1, a._2 + b._2));
		
		return counts.map(x -> String.format("%d,%d,%s,%d",x._1._1, x._1._2, STR_SCALE, x._2._2/x._2._1));
	}

	public static void main(String[] args) {
		SparkConf sparkConf = new SparkConf();
 		JavaSparkContext sc = new JavaSparkContext(sparkConf);

		JavaRDD<String> textFile = sc.textFile(logFile);
		Process(textFile).saveAsTextFile(outFile);
	}
}
