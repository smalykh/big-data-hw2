#!/bin/bash

N=10
NUMS=4
MAX_DATA=511

SEC_ADD=10

random_number() {
	echo $((($RANDOM % $NUMS) + 1))
}

random_time() {
	sec=`date +%s`

	sec_add=$(($RANDOM % $SEC_ADD))
	ms=$(($RANDOM % 1000))
	
	echo $(( ($sec + $sec_add)*1000 + $ms ))
}

random_data() {
	echo $((($RANDOM % $MAX_DATA) + 1))
}

for((i=0;i<N;i++))
do
	echo "$(random_number), $(random_time), $(random_data)"
done
