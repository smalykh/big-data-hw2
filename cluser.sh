#!/bin/bash
rm -rf out
hadoop dfs -rmr /out

spark-submit \
	--class "SimpleApp" \
	--master spark://sergei-VirtualBox:7077 \
	target/simple-project-1.0.jar

echo Result:
hdfs dfs -cat /out/*
